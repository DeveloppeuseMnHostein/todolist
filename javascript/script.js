//Récupérer les éléments

const inputBox = document.querySelector(".inputField input");
const addBtn = document.querySelector(".inputField button");
const todoList = document.querySelector(".todoList");
const deleteAllBtn = document.querySelector(".footer button");

inputBox.onkeyup = () => {
  let userData = inputBox.value; //pour que l'utilisateur rentre la donnée
  if (userData.trim() !== 0) {
    //pour que vérifier qu'il n'y a pas que des espaces
    addBtn.classList.add("active"); //pour rendre le bouton + actif
  } else {
    addBtn.classList.remove("active");
  }
};
showTasks(); //appel de la fonction

//Quand l'utilisateur clique sur le bouton "ajouter"
addBtn.onclick = () => {
  let userData = inputBox.value;
  let getLocalStorage = localStorage.getItem("New Todo"); //on récupère le stockage local
  if (getLocalStorage == null) {
    //S'il est vide, on installe un tableau vide
    listArray = [];
  } else {
    listArray = JSON.parse(getLocalStorage); // sinon on transforme les chaines de caractères JSON en objet
  }
  listArray.push(userData);
  localStorage.setItem("New Todo", JSON.stringify(listArray));
  showTasks();
  addBtn.classList.remove("active"); //désactive le addBtn
};

//Fonction pour que les taches s'affichent au fur et à mesure de l'enregistrement
function showTasks() {
  //   let userData = inputBox.value;
  let getLocalStorage = localStorage.getItem("New Todo"); //on récupère le stockage local
  if (getLocalStorage == null) {
    //S'il est vide, on installe un tableau vide
    listArray = [];
  } else {
    listArray = JSON.parse(getLocalStorage); // sinon on transforme les chaines de caractères JSON en objet
  }
  //Pour que le nombre de tache restante se mette à jour
  const pendingNumber = document.querySelector(".pendingNumber");
  pendingNumber.textContent = listArray.length;
  //Pour que si le nombre de tache est supérieur à zero, le bouton "vider la liste" soit actif
  if (listArray.length > 0) {
    deleteAllBtn.classList.add("active");
  } else {
    deleteAllBtn.classList.remove("active");
  }

  let newListTag = "";
  listArray.forEach((element, index) => {
    newListTag += `<li> ${element} <span onclick="deleteTask(${index})";>🗑</span></li>`;
  });
  todoList.innerHTML = newListTag; // ajout d'un élément de liste (li) dans la balise ul
  inputBox.value = ""; //pour remettre à vide l'input après l'enregistrement
}

//Fonction pour supprimer une tache
function deleteTask(index) {
  let getLocalStorage = localStorage.getItem("New Todo");
  listArray = JSON.parse(getLocalStorage);
  listArray.splice(index, 1); //supprime ou enlève un li en particulier (indexé?)
  //Après avoir enlevé un élément, on met à jour la liste
  localStorage.setItem("New Todo", JSON.stringify(listArray));
  showTasks();
}

//Fonction pour supprimer toutes les taches à la fois
deleteAllBtn.onclick = () => {
  listArray = []; //vider le tableau
  //mettre à jour
  localStorage.setItem("New Todo", JSON.stringify(listArray));
  showTasks();
};
